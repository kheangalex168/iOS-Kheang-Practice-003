//
//  PostService.swift
//  media-demo-9th
//
//  Created by Mavin on 13/11/21.
//

import Foundation
import Alamofire
import SwiftyJSON

struct PostService {
    static let shared = PostService()
    let postURL = "http://110.74.194.124:9999/api"
    
    let headers: HTTPHeaders = [
        "Content-Type": "Application/Json",
        "Authorization": "Bearer \(UserDefaults.standard.string(forKey: "key") ?? "")"
    ]
    
    func postArticle(title: String?, imageURL: String?, completion: @escaping(Result<String, Error>)->()){
        
        print("Bearer \(UserDefaults.standard.string(forKey: "key") ?? "")")
        
        
        let post: [String:Any] = [
            "caption": title ?? "no caption",
            "image": imageURL ?? "no image"
        ]
        
        AF.request("\(postURL)/posts/create", method: .post, parameters: post, encoding: JSONEncoding.default, headers: headers).response { response in
            if let error = response.error {
                completion(.failure(error))
            }else{
                
                guard let data = response.data else {
                    return
                }
                
                let jsonData = try! JSON(data: data)
                
                print("asdasdsaas \(jsonData)")
                
                completion(.success(jsonData["message"].stringValue))
            }
        }
        
    }
    
    
    func fetchPost(completion: @escaping (Result<[Post],Error>)->()){
        AF.request("\(postURL)/posts", headers: headers).response { response in
            
            switch response.result {
            case .success(let data):
                
                let jsonData = JSON(data)
                let payload = jsonData["payload"].arrayValue
                
                var posts: [Post] = []
                for postJSON in payload {
                    let post = Post(json: postJSON)
                    posts.append(post)
                }
                completion(.success(posts))
                print(payload)
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    
    func uploadImage(imageData: Data?,completion: @escaping(String?)->()){
        
        if let safeData = imageData {
            AF.upload(multipartFormData: { multiform in
                multiform.append(safeData, withName: "file", fileName: "ams", mimeType: "image/jpeg")
            }, to: "\(postURL)/files/upload", headers: headers).response { response in
                
                if let error = response.error{
                    print(error.localizedDescription)
                }
                
                print("asdsaasdas", response.data!)
                
                if let data = response.data{
                    
                    let jsonData = try! JSON(data: data)
                    
                    print("JsonDarta \(jsonData)")
                    
                    let url = jsonData["url"].stringValue
                    
                    completion(url)
                }
            }
        }else{
            completion(nil)
        }
    }
}
